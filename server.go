package main

import (
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/edast/developertest/externalservice"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"go.uber.org/zap"
)

type Server struct {
	client externalservice.Client
	router *mux.Router
	logger *zap.Logger
}

type ErrorRespose struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Path    string `json:"path,omitempty"`
}

func main() {
	s := NewServer(new(simpleClient))
	s.logger.Info("starting server on http://localhost:8080")
	http.ListenAndServe(":8080", s.router)
}

func writeErrorResponse(w http.ResponseWriter, statusCode int, msg string, path string) {
	w.WriteHeader(statusCode)
	resp := &ErrorRespose{
		Code:    statusCode,
		Message: msg,
		Path:    path,
	}
	str, _ := json.Marshal(resp)
	w.Write(str)
}

func (s *Server) RelayPost(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		s.logger.Debug("failed extracting id", zap.Error(err), zap.String("id", vars["id"]))
		writeErrorResponse(w, http.StatusBadRequest, "wrong id format", r.URL.Path)
		return
	}

	err = r.ParseForm()
	if err != nil {
		s.logger.Debug("failed parsing form", zap.Error(err))
		writeErrorResponse(w, http.StatusBadRequest, "failed parsing form", r.URL.Path)
		return
	}

	post := new(externalservice.Post)
	post.ID = id
	decoder := schema.NewDecoder()
	err = decoder.Decode(post, r.PostForm)
	if err != nil {
		s.logger.Debug("failed decoding form", zap.Error(err))
		writeErrorResponse(w, http.StatusBadRequest, "Failed decoding form", r.URL.Path)
		return
	}

	resp, err := s.client.POST(id, post)
	if err != nil {
		s.logger.Error("error while calling external client", zap.Error(err))
		writeErrorResponse(w, http.StatusInternalServerError, "Internal server error", r.URL.Path)
		return
	}

	str, err := json.Marshal(resp)
	if err != nil {
		s.logger.Error("error marshaling remote response", zap.Error(err))
		writeErrorResponse(w, http.StatusInternalServerError, "Internal server error", r.URL.Path)
		return
	}

	s.logger.Debug("relay post", zap.Any("submited post", post), zap.Any("returned post", resp))
	w.WriteHeader(http.StatusOK)
	w.Write(str)
}

func (s *Server) GetPost(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	w.Header().Set("Content-Type", "application/json")

	if err != nil {
		s.logger.Debug("failed extracting id", zap.Error(err), zap.String("id", vars["id"]))
		writeErrorResponse(w, http.StatusBadRequest, "wrong id format", r.URL.Path)
		return
	}

	resp, err := s.client.GET(id)
	if err != nil {
		s.logger.Error("error while calling external client", zap.Error(err))
		writeErrorResponse(w, http.StatusInternalServerError, "Internal server error", r.URL.Path)
		return
	}

	str, err := json.Marshal(resp)
	if err != nil {
		s.logger.Error("error marshaling remote response", zap.Error(err))
		writeErrorResponse(w, http.StatusInternalServerError, "Internal server error", r.URL.Path)
		return
	}

	s.logger.Debug("get post", zap.Int("id", id), zap.Any("returned post", resp))
	w.WriteHeader(http.StatusOK)
	w.Write(str)

}

func NewServer(client externalservice.Client) *Server {
	// should be changed to NewProduction in production
	logger, _ := zap.NewDevelopment()
	defer logger.Sync()

	r := mux.NewRouter()
	s := &Server{
		logger: logger,
		client: client,
		router: r,
	}

	r.HandleFunc("/api/posts/{id}", s.RelayPost).Methods("POST")
	r.HandleFunc("/api/posts/{id}", s.GetPost).Methods("GET")
	return s
}

// mockClient for local dev testing
type simpleClient struct {
}

func (m *simpleClient) GET(id int) (*externalservice.Post, error) {
	return &externalservice.Post{
		ID:          id,
		Title:       "foo",
		Description: "bar",
	}, nil
}

func (m *simpleClient) POST(id int, post *externalservice.Post) (*externalservice.Post, error) {
	return post, nil
}
