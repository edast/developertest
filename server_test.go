package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"strings"
	"testing"

	"bitbucket.org/edast/developertest/externalservice"
	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

func TestPOSTCallsAndReturnsJSONfromExternalServicePOST(t *testing.T) {
	// Descirption
	//
	// Write a test that accepts a POST request on the server and sends it the
	// fake external service with the posted form body return the response.
	//
	// Use the externalservice.Client interface to create a mock and assert the
	// client was called <n> times.
	//
	// ---
	//
	// Server should receive a request on
	//
	//  [POST] /api/posts/:id
	//  application/json
	//
	// With the form body
	//
	//  application/x-www-form-urlencoded
	//	title=Hello World!
	//	description=Lorem Ipsum Dolor Sit Amen.
	//
	// The server should then relay this data to the external service by way of
	// the Client POST method and return the returned value out as JSON.
	//
	// ---
	//
	// Assert that the externalservice.Client#POST was called 1 times with the
	// provided `:id` and post body and that the returned Post (from
	// externalservice.Client#POST) is written out as `application/json`.
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	type args struct {
		path string
		id   int
		form url.Values
	}
	tests := []struct {
		name     string
		args     args
		respCode int
	}{
		{
			name: "test post is working",
			args: args{
				path: "/api/posts/",
				id:   123,
				form: url.Values{
					"title":       []string{"Hello World!"},
					"description": []string{"Lorem Ipsum Dolor Sit Amen."},
				},
			},
			respCode: http.StatusOK,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			mockClient := externalservice.NewMockClient(mockCtrl)
			s := NewServer(mockClient)
			mockClient.EXPECT().POST(tt.args.id, gomock.Any()).Times(1)

			url := fmt.Sprintf("%s/%d", tt.args.path, tt.args.id)
			req, err := http.NewRequest("POST", url, strings.NewReader(tt.args.form.Encode()))
			if err != nil {
				t.Fatal(err)
			}
			req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
			req.Header.Add("Content-Length", strconv.Itoa(len(tt.args.form.Encode())))
			req = mux.SetURLVars(req, map[string]string{"id": strconv.Itoa(tt.args.id)})

			rr := httptest.NewRecorder()
			handler := http.HandlerFunc(s.RelayPost)

			handler.ServeHTTP(rr, req)

			assert.Equal(t, tt.respCode, rr.Code)
		})
	}
}

func TestPOSTCallsAndReturnsErrorAsJSONFromExternalServiceGET(t *testing.T) {
	// Description
	//
	// Write a test that accepts a GET request on the server and returns the
	// error returned from the external service.
	//
	// Use the externalservice.Client interface to create a mock and assert the
	// client was called <n> times.
	//
	// ---
	//
	// Server should receive a request on
	//
	//	[GET] /api/posts/:id
	//
	// The server should then return the error from the external service out as
	// JSON.
	//
	// The error response returned from the external service would look like
	//
	//	400 application/json
	//
	//	{
	//		"code": 400,
	//		"message": "Bad Request"
	//	}
	//
	// ---
	//
	// Assert that the externalservice.Client#GET was called 1 times with the
	// provided `:id` and the returned error (above) is output as the response
	// as
	//
	//	{
	//		"code": 400,
	//		"message": "Bad Request",
	//		"path": "/api/posts/:id
	//	}
	//
	// Note: *`:id` should be the actual `:id` in the original request.*

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	type args struct {
		path string
		id   int
	}
	type fields struct {
		err  error
		post *externalservice.Post
	}
	type want struct {
		err     bool
		errResp *ErrorRespose
	}
	tests := []struct {
		name     string
		args     args
		respCode int
		fields   fields
		want     want
	}{
		{
			name: "Get is working",
			args: args{
				path: "/api/posts/",
				id:   123,
			},
			respCode: http.StatusOK,
			fields: fields{
				err: nil,
				post: &externalservice.Post{
					ID: 123,
				},
			},
			want: want{
				err:     false,
				errResp: nil,
			},
		},
		{
			name: "Get returns error on client error",
			args: args{
				path: "/api/posts/",
				id:   123,
			},
			respCode: http.StatusInternalServerError,
			fields: fields{
				err:  errors.New("some bad error"),
				post: nil,
			},
			want: want{
				err: true,
				errResp: &ErrorRespose{
					Code:    http.StatusInternalServerError,
					Message: "Internal server error",
					Path:    "/api/posts/123",
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			mockClient := externalservice.NewMockClient(mockCtrl)
			s := NewServer(mockClient)
			mockClient.EXPECT().GET(tt.args.id).
				Return(tt.fields.post, tt.fields.err).Times(1)

			url := fmt.Sprintf("%s/%d", tt.args.path, tt.args.id)
			req, err := http.NewRequest("GET", url, nil)
			if err != nil {
				t.Fatal(err)
			}
			req.Header.Add("Content-Type", "application/json")
			req = mux.SetURLVars(req, map[string]string{"id": strconv.Itoa(tt.args.id)})

			rr := httptest.NewRecorder()
			handler := http.HandlerFunc(s.GetPost)

			handler.ServeHTTP(rr, req)
			assert.Equal(t, tt.respCode, rr.Code)

			if tt.want.err {
				var resp *ErrorRespose
				err := json.NewDecoder(rr.Body).Decode(&resp)
				if err != nil {
					t.Error(err)
					t.Fail()
				}
				assert.Equal(t, tt.respCode, resp.Code)
			} else {
				var p *externalservice.Post
				err := json.NewDecoder(rr.Body).Decode(&p)
				if err != nil {
					t.Error(err)
					t.Fail()
				}
				assert.Equal(t, tt.fields.post, p)
			}
		})
	}
}
