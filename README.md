Make sure you have dep installed https://github.com/golang/dep

```
git clone https://bitbucket.org/edast/developertest $GOPATH/src/bitbucket.org/edast/developertest
```

Then execute:
```
make test
make run
```